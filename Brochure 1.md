# Free Software - Leafelet Content

## Section 1 - The Basics

As our society grows more dependent on computers, the software we run is of critical importance to securing the future of a free society. Free software is about having control over the technology we use in our homes, schools and businesses, where computers work for our individual and communal benefit, not for proprietary software companies or governments who might seek to restrict and monitor us. 

> Digital technology can give you freedom; it can also take your freedom away.

### Why should you use free software?

    1. It is inherently more Stable and Secure.
    
    Why ? because the code is open to the public. any errors / security failure in part of the program will be caught sooner by the large number of eyes reviewing and testing the code. 
    This is unlike propreitary software, where the details of the software are closely guarded. As a result of this, there is no guaranetee whatsoever that the software you rely on is secure or stable. It might even be a surveillance software for all one can say.  


    1. Free as In Freedom. 
    
    Free in terms of money, should be the least of the reasons why you should switch over to free software. But then, most of the Free software are free to use and distribute. But it is important to understand Free Software and its philosophy.

    “Free software” means software that respects user's freedom and community. Roughly, it means that the users have the freedom to run, copy, distribute, study, change and improve the software. Thus, “free software” is a matter of liberty, not price. To understand the concept, you should think of “free” as in “free speech,” not as in “free food”. It issometimes called “libre software,” borrowing the French or Spanish word for “free” as in freedom, to show we do not mean the software is free of charge. 

#### Here are the four fundamental principles of free software


    1. The freedom to run the program as you wish, for any purpose (freedom 0).
    1. The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.
    1. The freedom to redistribute copies so you can help others (freedom 2).
    1. The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

--------------------

## Story of linux and mozilla - the role that they played in transforming the web.

## Why Educational Institutions Should Use and Teach Free Software

    https://www.gnu.org/education/edu-why.html

## Why should I be using free software. What is the problem with proprietary / closed source software ?

They create a dependancy system. ( no interoperability with other systems)

Today, free software is available for just about any task you can imagine. From complete operating systems like GNU, to over 5,000 individual programs and tools listed in the FSF/UNESCO free software directory. 
Millions of people around the world—including entire governments—are now using free software on their computers.

As our society grows more dependent on computers, the software we run is of critical importance to securing the future of a free society. Free software is about having control over the technology we use in our homes, schools and businesses, where computers work for our individual and communal benefit, not for proprietary software companies or governments who might seek to restrict and monitor us.

## Gnu and Linux. The distinction.

## The Capabilities of Free Software - Comparison Table

## Privacy and Control over your own data

To use free software is to make a political and ethical choice asserting the right to learn, and share what we learn with others.

Many freedoms and benefits that free software provides it users are outright denied by equivalent proprietary softwares.

In the yester years, right to land, quality food, education were important. in this new age of access to knowledge of computers, and how their inners works, access to the code  that runs the programs work, etc are of paramount importance.

In this day and age, our computers control much of our personal information and daily activities.  corporations behind proprietary software will often spy on your activities and restrict you from sharing with others. proprietary software represents an unacceptable danger to a free society.

## Fundamental digital rights

1. Right to Change Providers
    If a person wants to change a service provider, they can easily move to another. (Decentralized Services)
2. Right to Protect Personal Data
    A person owns and controls their own master keys to encrypt all data and communication, nobody else. (User-controlled Encryption)
3. Right to Verify
    Society has the freedom to inspect the source of all software used, and can run it as they wish, for any purpose. (Software Freedom)
4. Right to be Forgotten
    A service provider only stores the minimal personal data necessary to provide the service. Once the data is no longer required, it is deleted. (Minimal Data Retention)
5. Right to Access
    A person must not be discriminated against nor forced to agree to any terms and conditions before accessing a service. (Personal Liberty)

If we can do those things, we can change the future of computing for the better.

Source : Todd Weaver, Founder Puri.sm

## Invitation to join the movement

There exists a movement in the world, where a worldwide group of talented ethical programmers voluntarily committed to the idea of writing and sharing software with each other and with anyone else who agreed to share alike and you can be part of it. Anyone could be a part of and benefit from this community even without being a computer expert or knowing anything about programming. We wouldn’t have to worry about getting caught copying a useful program for our friends—because we wouldn’t be doing anything wrong

## imporatnt links

    - free software foundation
    - FSF India
    - FSUG India

FSF/UNESCO free software directory
https://www.gnu.org/

Free Software Foundation of India
http://gnu.org.in/

## connect locally with us

Free Sofwarew User Group - Thiruvananthapuram

    - #fsug-tvm:matrix.org (Telegram)
    - https://t.me/fsugtvm (Matrix)

-----------------------------------

## logos of different entities
